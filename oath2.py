from fastapi import Depends,HTTPException,status
from fastapi.security import OAuth2PasswordBearer
from config import get_db,SECRET_KEY,ALGORITHM
from sqlalchemy.orm import Session
from jose import JWTError, jwt
import model,schema



oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# def get_current_user(token: str = Depends(oauth2_scheme)):
def get_current_user(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email = payload.get("sub")
        # id = payload.get("id")
        
        if email is None:
            raise HTTPException(status_code=401, detail="Invalidd authentication credentials")
        # tokendata=email
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid authentication credentials")
    # user=crud.get_by_email(db,email=tokendata)

    return payload


