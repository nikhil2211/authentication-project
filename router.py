from fastapi import APIRouter, Depends,HTTPException,status,Query
from typing import Optional,List
from fastapi.responses import JSONResponse
import re
from schema import  ResponseSchema, TokenResponse,User
from sqlalchemy.orm import Session
from config import get_db, ACCESS_TOKEN_EXPIRE_MINUTES
from passlib.context import CryptContext
from repository import JWTRepo,Hash
from model import Users
from datetime import datetime, timedelta
import schema,model
from fastapi_jwt_auth import AuthJWT
import oath2

router = APIRouter()

# encrypt password
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


"""
    Authentication Router

"""


@router.post('/signup')
async def signup(request: schema.User, db: Session = Depends(get_db)):
    hashed_password=pwd_context.hash(request.password)
    if request.name is None or request.name == "":
        raise HTTPException(status_code=400, detail="Name cannot be blank")

    if not re.match(r"[^@]+@[^@]+\.[^@]+", request.email):
        raise HTTPException(status_code=400, detail="Email is not valid")

    db_item = db.query(model.Users).filter(model.Users.email == request.email).first()
    if db_item:
        raise HTTPException(status_code=400, detail="Email already registered")

    new_user=model.Users(name=request.name,email=request.email,password=hashed_password)
    db.add(new_user)
    db.commit()
    # db.refresh()
    # return new_user
    token = JWTRepo.generate_token({"sub": new_user.id})
    return ResponseSchema(code="200", status="OK", message="success Signup!", data=TokenResponse(access_token=token, token_type="Bearer",user=[{"id":new_user.id,"name":new_user.name,"email":new_user.email}])).dict(exclude_none=True)
    
    
    
@router.post('/login')
async def login(request: schema.LoginUser, db: Session = Depends(get_db)):
    
    if not re.match(r"[^@]+@[^@]+\.[^@]+", request.email):
        raise HTTPException(status_code=400, detail="Email is not valid")
    
    obj = db.query(model.Users).filter(model.Users.email == request.email).first()
    if not obj:
        raise HTTPException(status_code=400, detail="Email not registered")

    if not Hash.verify(request.password,obj.password):
            return ResponseSchema(code="400", status="Bad Request", message="Invalid password").dict(exclude_none=True)
    
    if Hash.verify(request.password,obj.password) :
        token = JWTRepo.generate_token({"sub": obj.email,"id":obj.id})
        return ResponseSchema(code="200", status="OK", message="success Login!", data=TokenResponse(access_token=token, token_type="Bearer",user=[{"id":obj.id,"name":obj.name,"email":obj.email}])).dict(exclude_none=True) 

    
@router.get("/user")
def read_user_me(db:Session=Depends(get_db) ,current_user:str=Depends(oath2.get_current_user)):
    email= current_user.get("sub")
    
    data= db.query(model.Users).filter(model.Users.email==email).first()
    return ResponseSchema(code="200", status="OK", message="User detail", data={"id":data.id,"name":data.name,"email":data.email}).dict(exclude_none=True)

@router.post("/contact")
def create_new_contact(request: schema.ConTact, db: Session = Depends(get_db),current_user:str=Depends(oath2.get_current_user)):
    if request.name is None or request.name == "":
        raise HTTPException(status_code=400, detail="Name cannot be blank")
    
    if request.phone is None or request.phone == "":
        raise HTTPException(status_code=400, detail="Phone No. cannot be blank")
    
    
    db_item = db.query(model.Contacts).filter(model.Contacts.phone == request.phone).first()
    if db_item:
        raise HTTPException(status_code=400, detail="Phone No. already Present In the Contact List")
    
    
    new_contact=model.Contacts(name=request.name,phone=request.phone,email=request.email,country=request.country,address=request.address,owner_id=current_user.get("id"))
    db.add(new_contact)
    db.commit()
    
    return ResponseSchema(code="200", status="OK", message="Contact added", data={"id":new_contact.id,"name":new_contact.name,"email":new_contact.email,"phone":new_contact.phone,"country":new_contact.country,"address":new_contact.address}).dict(exclude_none=True)

    
    
    
    
    
    
    
# @router.get("/paginate",dependencies=[Depends(oath2.get_current_user)])
# def get_paginated_list(page: Optional[int] = Query(1, ge=1), page_size: Optional[int] = Query(10, ge=1, le=100), db: Session = Depends(get_db)):
#     list_item=[]
#     """
#     Returns a list of items, paginated by the provided page and page_size parameters.
#     """
#     limit=page_size
#     offset = (page - 1) * page_size
#     items= db.query(model.Contacts).offset(offset).limit(page_size).all()
#     for item in items:
#         list_item.append(item)
        
#     total=len(db.query(model.Contacts).all())
#     has_next = (offset + limit) < total
#     has_prev = offset > 0
#     pages = (total // limit) + int(total % limit > 0)
#     return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact Lists":list_item,"has_next":has_next,"has_prev":has_prev,"page":page,"pages":pages,"per_page":page_size,"total":total}).dict(exclude_none=True)



# @router.get('/sortcontacts',dependencies=[Depends(oath2.get_current_user)])
# def get_contacts(sort_by: str = 'latest', db: Session = Depends(get_db)):
#     list_item=[]
#     if sort_by == 'latest':
#         contacts = db.query(model.Contacts).order_by(model.Contacts.create_date.desc()).all()
#     elif sort_by == 'oldest':
#         contacts = db.query(model.Contacts).order_by(model.Contacts.create_date.asc()).all()
#     elif sort_by == 'a_to_z':
#         contacts = db.query(model.Contacts).order_by(model.Contacts.name.asc()).all()
#     elif sort_by == 'z_to_a':
#         contacts = db.query(model.Contacts).order_by(model.Contacts.name.desc()).all()
#     else:
#         return JSONResponse(content={'error': 'Invalid sort_by parameter. Must be one of latest, oldest, or alphabetical.'}, status_code=400)
    
#     for item in contacts:
#         list_item.append(item)
    
    
#     return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact Lists":list_item}).dict(exclude_none=True)



# @router.get("/search",dependencies=[Depends(oath2.get_current_user)])
# async def search_questions(search_by: str, search_term: str,db:Session=Depends(get_db)):
#     if search_by not in ["name", "email", "phone"]:
#         return {"error": "Invalid search_by parameter"}
    
#     if search_by=='name':
#         obj= db.query(model.Contacts).filter(model.Contacts.name.ilike(f"%{search_term}%")).all()
    
#     if search_by=='email':
#         obj= db.query(model.Contacts).filter(model.Contacts.email.ilike(f"%{search_term}%")).all()
    
#     if search_by=='phone':
#         obj= db.query(model.Contacts).filter(model.Contacts.phone.ilike(f"%{search_term}%")).all()
    
#     return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact Lists":obj}).dict(exclude_none=True)



@router.get("/contact/",dependencies=[Depends(oath2.get_current_user)])
def paginate(search_by:  Optional[str] = None, search_term:  Optional[str] = None,page: Optional[int] = Query(1, ge=1), page_size: Optional[int] = Query(10, ge=1, le=100),db: Session = Depends(get_db),sort_by:  Optional[str] = None):
    # ,sort_by: str = 'latest'
    obj_list=[]
    """
    Returns a list of items, paginated by the provided page and page_size parameters.
    """
    if search_by and search_term:
        if search_by not in ["name", "email", "phone"]:
            return {"error": "Invalid search_by parameter"}
        
        if search_by=='name':
            obj= db.query(model.Contacts).filter(model.Contacts.name.ilike(f"%{search_term}%")).all()
        
        if search_by=='email':
            obj= db.query(model.Contacts).filter(model.Contacts.email.ilike(f"%{search_term}%")).all()
        
        if search_by=='phone':
            obj= db.query(model.Contacts).filter(model.Contacts.phone.ilike(f"%{search_term}%")).all()

        for item in obj:
            obj_list.append(item)
        
        return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact List":obj_list}).dict(exclude_none=True)
    
    if sort_by :
        list_item=[]
        if sort_by == 'latest':
            contacts = db.query(model.Contacts).order_by(model.Contacts.create_date.desc()).all()
        elif sort_by == 'oldest':
            contacts = db.query(model.Contacts).order_by(model.Contacts.create_date.asc()).all()
        elif sort_by == 'a_to_z':
            contacts = db.query(model.Contacts).order_by(model.Contacts.name.asc()).all()
        elif sort_by == 'z_to_a':
            contacts = db.query(model.Contacts).order_by(model.Contacts.name.desc()).all()
        else:
            return JSONResponse(content={'error': 'Invalid sort_by parameter. Must be one of latest, oldest, or alphabetical.'}, status_code=400)
        
        for item in contacts:
            list_item.append(item)
        
        
        return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact Lists":list_item}).dict(exclude_none=True)
    
    else:
        list_con=[]
        limit=page_size
        offset = (page - 1) * page_size
        items= db.query(model.Contacts).offset(offset).limit(page_size).all()
        for item in items:
            list_con.append(item)
            
        total=len(db.query(model.Contacts).all())
        has_next = (offset + limit) < total
        has_prev = offset > 0
        pages = (total // limit) + int(total % limit > 0)
        return ResponseSchema(code="200", status="OK", message="Contacts", data={"Contact Listss":list_con,"has_next":has_next,"has_prev":has_prev,"page":page,"pages":pages,"per_page":page_size,"total":total}).dict(exclude_none=True)

        