from pydantic import BaseModel
from typing import Optional,TypeVar

T = TypeVar('T')


class User(BaseModel):
    name:str
    email:str
    password:str
    
class LoginUser(BaseModel):
    email:str
    password:str
    
class TokenResponse(BaseModel):
    access_token :str
    token_type: str
    user:list
    
    
class ResponseSchema(BaseModel):
    code: str
    status: str
    message: str
    data: Optional[T] = None 
    # user:list
    
# class UserOutput(BaseModel):
#     id:int
#     name:str
#     email:str
    
    
    
    
# class TokenData(BaseModel):
#     username : Optional[str]=None


# class UserLoggedin(BaseModel):
#     id:int
#     name:str
#     email:str



# class TokenData(BaseModel):
#     email:Optional[str]=None


class ConTact(BaseModel):
    name:str
    email:str
    phone:str
    country:str
    address:str
    # owner_id:int
    