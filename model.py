import email
from sqlalchemy import Column, Integer, String, Boolean, DateTime,ForeignKey
from config import Base
from sqlalchemy.orm import relationship
import datetime

class Users(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)
    email = Column(String)
    phone_number = Column(String)
    
    post=relationship("Contacts",back_populates="owner")
    
    
class Contacts(Base):
    __tablename__ = 'contacts' 

    id = Column(Integer, primary_key=True)
    name = Column(String)
    phone = Column(String)
    email = Column(String)
    country = Column(String)
    address = Column(String)  
    owner_id=Column(Integer, ForeignKey("users.id"))
    create_date = Column(DateTime, default=datetime.datetime.now())
    
    
    owner=relationship("Users",back_populates="post")


    
