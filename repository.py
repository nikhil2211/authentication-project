from typing import TypeVar, Generic, Optional
from sqlalchemy.orm import Session

from datetime import datetime, timedelta
from jose import JWTError, jwt
from config import SECRET_KEY, ALGORITHM,get_db

from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Request, HTTPException
from passlib.context import CryptContext
import schema,model



pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")



class JWTRepo():
    
    def generate_token(data: dict, expires_delta: Optional[timedelta] = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=15)
        to_encode.update({"exp": expire})
        encode_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        
        return encode_jwt

    # def decode_token(token: str):
    #     try:
    #         decode_token = jwt.decode(token, SECRET_KEY, algorithm=[ALGORITHM])
    #         return decode_token if decode_token["expires"] >= datetime.time() else None
    #     except:
    #         return{}
        
    # def verify_token(token:str,credentials_exception):
    #     try:
    #         payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    #         username: str = payload.get("sub")
    #         if username is None:
    #             raise credentials_exception
    #         token_data = schema.TokenData(email=email)
    #     except JWTError:
    #         raise credentials_exception
    
    # def get_current_user(db: Session = Depends(get_db),token=Depends(OAuth2schema)):
    #     pass


# class JWTBearer(HTTPBearer):

#     def __init__(self, auto_error: bool = True):
#         super(JWTBearer, self).__init__(auto_error=auto_error)

#     async def __call__(self, request: Request):
#         credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)

#         if credentials:
#             if not credentials.scheme == "Bearer":
#                 raise HTTPException(
#                     status_code=403, detail="Invalid authentication sheme.")
#             if self.verfity_jwt(credentials.credentials):
#                 raise HTTPException(
#                     status_code=403, detail="Invalid token or expiredd token.")
#             return credentials.credentials
#         else:
#             raise HTTPException(
#                 status=403, detail="Invalid authorization code.")

#     def verfity_jwt(Self, jwttoken: str):
#         isTokenValid: bool = False

#         try:
#             payload = jwt.decode(jwttoken, SECRET_KEY, algorithm=[ALGORITHM])
#         except:
#             payload = None

#         if payload:
#             isTokenValid = True
#         return isTokenValid
    
    


# class BaseRepo():

#     @staticmethod
#     def retrieve_all(db: Session, model: Generic[T]):
#         return db.query(model).all()
    
    
#     @staticmethod
#     def retrieve_by_id(db: Session, model: Generic[T], id: int):
#         return db.query(model).filter(model.id == id).all()

#     @staticmethod
#     def insert(db: Session, model: Generic[T]):
#         db.add(model)
#         db.commit()
#         db.refresh(model)

#     @staticmethod
#     def update(db: Session, model: Generic[T]):
#         db.commit()
#         db.refresh(model)

#     @staticmethod
#     def delete(db: Session, model: Generic[T]):
#         db.delete(model)
#         db.commit()
    


T = TypeVar('T')
    

# class UsersRepo():
        
#     @staticmethod
#     def find_by_email(db:Session, model: Generic[T], email: str):
#         return db.query(model).filter(model.email == email).first()

class Hash():
    def verify(plain_password,hashes_password):
        return pwd_context.verify(plain_password,hashes_password)

