# Project Name : Authentication Project




# Project configuration

1. install python3.9
2. install virtualenv
3. activate virtualenv   : source venvv/local/bin/activate

4. install all the requirement from requirement.txt
pip install -r requirement.txt
5. type command uvicorn main:app to run the project

5. database is postgresql
setup db credential in configs.py

